﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JDConnector.Controllers
{
    public class ConnectorController : ApiController
    {
        /* 
         * Local IIS 
 http://localhost/JDConnector/api/Connector/GetJDData?PostJDEXMLRequest=<?xml version='1.0' encoding='UTF-8' ?>
<jdeRequest>
	<callMethod app='XMLTest' name='GetUDC'>
		<params>
			<param name='szSystemCode'>01</param>
			<param name='szRecordTypeCode'>01</param>
			<param name='szUserDefinedCode'>CCP</param>
			<param name='cSuppressErrorMessage'>1</param>
		</params>
	</callMethod>
</jdeRequest>

 //Local IIS Express
 http://localhost:60576/api/Connector/GetJDData?PostJDEXMLRequest=%3C?xml%20version=%271.0%27%20encoding=%27UTF-8%27%20?%3E%20%3CjdeRequest%3E%20%3CcallMethod%20app=%27XMLTest%27%20name=%27GetUDC%27%3E%20%3Cparams%3E%20%3Cparam%20name=%27szSystemCode%27%3E01%3C/param%3E%20%3Cparam%20name=%27szRecordTypeCode%27%3E01%3C/param%3E%20%3Cparam%20name=%27szUserDefinedCode%27%3ECCP%3C/param%3E%20%3Cparam%20name=%27cSuppressErrorMessage%27%3E1%3C/param%3E%20%3C/params%3E%20%3C/callMethod%3E%20%3C/jdeRequest%3E
      
http://localhost:60576/api/Connector/GetJDData?PostJDEXMLRequest=<?xml version="1.0" encoding="UTF-8"?>
<jdeRequest>
  <callMethod app="MobileSales" name="AddressBookMasterMBF">
    <params>
      <param name="cActionCode" id="ABIp1">I</param>
      <param name="cUpdateMasterFile" id="ABIp2">
      </param>
      <param name="cProcessEdits" id="ABIp3">
      </param>
      <param name="cSuppressErrorMessages" id="ABIp4">
      </param>
      <param name="szErrorMessageID" id="ABIp5">
      </param>
      <param name="szVersion" id="ABIp6">
      </param>
      <param name="mnSameAsExcept" id="ABIp7">
      </param>
      <param name="mnAddressBookNumber" id="ABIp8">1</param>
      <param name="szLongAddressNumber" id="ABIp9">
      </param>
      <param name="szTaxId" id="ABIp10">
      </param>
      <param name="szSearchType" id="ABIp11">
      </param>
      <param name="szAlphaName" id="ABIp12">
      </param>
      <param name="szSecondaryAlphaName" id="ABIp13">
      </param>
      <param name="szMailingName" id="ABIp14">
      </param>
      <param name="szSecondaryMailingName" id="ABIp15">
      </param>
      <param name="szDescriptionCompressed" id="ABIp16">
      </param>
      <param name="szBusinessUnit" id="ABIp17">
      </param>
      <param name="szAddressLine1" id="ABIp18">
      </param>
      <param name="szAddressLine2" id="ABIp19">
      </param>
      <param name="szAddressLine3" id="ABIp20">
      </param>
      <param name="szAddressLine4" id="ABIp21">
      </param>
      <param name="szPostalCode" id="ABIp22">
      </param>
      <param name="szCity" id="ABIp23">
      </param>
      <param name="szCounty" id="ABIp24">
      </param>
      <param name="szState" id="ABIp25">
      </param>
      <param name="szCountry" id="ABIp26">
      </param>
      <param name="szCarrierRoute" id="ABIp27">
      </param>
      <param name="szBulkMailingCenter" id="ABIp28">
      </param>
      <param name="szPrefix1" id="ABIp29">
      </param>
      <param name="szPhoneNumber1" id="ABIp30">
      </param>
      <param name="szPhoneNumberType1" id="ABIp31">
      </param>
      <param name="szPhoneAreaCode2" id="ABIp32">
      </param>
      <param name="szPhoneNumber2" id="ABIp33">
      </param>
      <param name="szPhoneNumberType2" id="ABIp34">
      </param>
      <param name="cPayablesYNM" id="ABIp35">
      </param>
      <param name="cReceivablesYN" id="ABIp36">
      </param>
      <param name="cEmployeeYN" id="ABIp37">
      </param>
      <param name="cUserCode" id="ABIp38">
      </param>
      <param name="cARAPNettingY" id="ABIp39">
      </param>
      <param name="cSubledgerInactiveCode" id="ABIp40">
      </param>
      <param name="cPersonCorporationCode" id="ABIp41">
      </param>
      <param name="szCertificate" id="ABIp42">
      </param>
      <param name="szAddlIndTaxID" id="ABIp43">
      </param>
      <param name="szCreditMessage" id="ABIp44">
      </param>
      <param name="szLanguage" id="ABIp45">
      </param>
      <param name="szIndustryClassification" id="ABIp46">
      </param>
      <param name="cEMail" id="ABIp47">
      </param>
      <param name="mn1stAddressNumber" id="ABIp48">
      </param>
      <param name="mn2ndAddressNumber" id="ABIp49">
      </param>
      <param name="mn3rdAddressNumber" id="ABIp50">
      </param>
      <param name="mn4thAddressNumber" id="ABIp51">
      </param>
      <param name="mn5thAddressNumber" id="ABIp52">
      </param>
      <param name="mnFactorSpecialPayee" id="ABIp53">
      </param>
      <param name="mnParentNumber" id="ABIp54">
      </param>
      <param name="cAddressType3YN" id="ABIp55">
      </param>
      <param name="cAddressType4YN" id="ABIp56">
      </param>
      <param name="cAddressType5YN" id="ABIp57">
      </param>
      <param name="szCategoryCode01" id="ABIp58">
      </param>
      <param name="szAccountRepresentative" id="ABIp59">
      </param>
      <param name="szCategoryCode03" id="ABIp60">
      </param>
      <param name="szGeographicRegion" id="ABIp61">
      </param>
      <param name="szCategoryCode05" id="ABIp62">
      </param>
      <param name="szCategoryCode06" id="ABIp63">
      </param>
      <param name="sz1099Reporting" id="ABIp64">
      </param>
      <param name="szCategoryCode08" id="ABIp65">
      </param>
      <param name="szCategoryCode09" id="ABIp66">
      </param>
      <param name="szCategoryCode10" id="ABIp67">
      </param>
      <param name="szSalesRegion" id="ABIp68">
      </param>
      <param name="szCategoryCode12" id="ABIp69">
      </param>
      <param name="szLineOfBusiness" id="ABIp70">
      </param>
      <param name="szSalesVolume" id="ABIp71">
      </param>
      <param name="szCategoryCode15" id="ABIp72">
      </param>
      <param name="szCategoryCode16" id="ABIp73">
      </param>
      <param name="szCategoryCode17" id="ABIp74">
      </param>
      <param name="szCategoryCode18" id="ABIp75">
      </param>
      <param name="szCategoryCode19" id="ABIp76">
      </param>
      <param name="szCategoryCode20" id="ABIp77">
      </param>
      <param name="szCategoryCode21" id="ABIp78">
      </param>
      <param name="szCategoryCode22" id="ABIp79">
      </param>
      <param name="szCategoryCode23" id="ABIp80">
      </param>
      <param name="szCategoryCode24" id="ABIp81">
      </param>
      <param name="szCategoryCode25" id="ABIp82">
      </param>
      <param name="szCategoryCode26" id="ABIp83">
      </param>
      <param name="szCategoryCode27" id="ABIp84">
      </param>
      <param name="szCategoryCode28" id="ABIp85">
      </param>
      <param name="szCategoryCode29" id="ABIp86">
      </param>
      <param name="szCategoryCode30" id="ABIp87">
      </param>
      <param name="szGlBankAccount" id="ABIp88">
      </param>
      <param name="mnTimeScheduledIn" id="ABIp89">
      </param>
      <param name="jdDateScheduledIn" id="ABIp90">
      </param>
      <param name="cClearedY" id="ABIp91">
      </param>
      <param name="szRemark" id="ABIp92">
      </param>
      <param name="szUserReservedCode" id="ABIp93">
      </param>
      <param name="jdUserReservedDate" id="ABIp94">
      </param>
      <param name="mnUserReservedAmount" id="ABIp95">
      </param>
      <param name="mnUserReservedNumber" id="ABIp96">
      </param>
      <param name="szUserReservedReference" id="ABIp97">
      </param>
      <param name="jdDateEffective" id="ABIp98">
      </param>
      <param name="szProgramId" id="ABIp99">
      </param>
      <param name="szRemark1" id="ABIp100">
      </param>
      <param name="mnAddNumParentOriginal" id="ABIp101">
      </param>
      <param name="OKToDelete" id="ABIp102">
      </param>
      <param name="szVersionconsolidated" id="ABIp103">
      </param>
      <param name="cDirectionIndicator" id="ABIp104">
      </param>
      <param name="cEdiSuccessfullyProcess" id="ABIp105">
      </param>
      <param name="szCountryForPayroll" id="ABIp106">
      </param>
      <param name="szShortcutClientType" id="ABIp107">
      </param>
      <param name="szTicker" id="ABIp108">
      </param>
      <param name="szStockExchange" id="ABIp109">
      </param>
      <param name="szDUNSNumber" id="ABIp110">
      </param>
      <param name="szClassificationCode01" id="ABIp111">
      </param>
      <param name="szClassificationCode02" id="ABIp112">
      </param>
      <param name="szClassificationCode03" id="ABIp113">
      </param>
      <param name="szClassificationCode04" id="ABIp114">
      </param>
      <param name="szClassificationCode05" id="ABIp115">
      </param>
      <param name="mnNumberOfEmployee" id="ABIp116">
      </param>
      <param name="mnGrowthRate" id="ABIp117">
      </param>
      <param name="szYearStarted" id="ABIp118">
      </param>
      <param name="szEmployeeGroupApprovals" id="ABIp119">
      </param>
      <param name="cIndicatorFlg" id="ABIp120">
      </param>
      <param name="szRevenueRange" id="ABIp121">
      </param>
      <param name="mnABSynchStatus" id="ABIp122">
      </param>
      <param name="mnABServerStatus" id="ABIp123">
      </param>
      <param name="mnPhone1SynchStatus" id="ABIp124">
      </param>
      <param name="mnPhone1ServerStatus" id="ABIp125">
      </param>
      <param name="mnPhone2SynchStatus" id="ABIp126">
      </param>
      <param name="mnPhone2ServerStatus" id="ABIp127">
      </param>
      <param name="mnAddressSynchStatus" id="ABIp128">
      </param>
      <param name="mnAddressServerStatus" id="ABIp129">
      </param>
      <param name="mnParentAddSynchStatus" id="ABIp130">
      </param>
      <param name="mnPreviousF0101ErrorStatus" id="ABIp131">
      </param>
      <param name="mnWhosWhoSynchStatus" id="ABIp132">
      </param>
      <param name="mnWhosWhoServerStatus" id="ABIp133">
      </param>
      <param name="cCallSalesTeamAlignment" id="ABIp134">
      </param>
    </params>
  </callMethod>
  <callMethod app="MobileSales" name="AddressBookMasterMBF">
    <params>
      <param name="cActionCode">C</param>
      <param name="cUpdateMasterFile">1</param>
      <param name="cProcessEdits" idref="ABIp3">
      </param>
      <param name="cSuppressErrorMessages" idref="ABIp4">
      </param>
      <param name="szErrorMessageID" idref="ABIp5">
      </param>
      <param name="szVersion" idref="ABIp6">
      </param>
      <param name="mnSameAsExcept" idref="ABIp7">
      </param>
      <param name="mnAddressBookNumber" idref="ABIp8">
      </param>
      <param name="szLongAddressNumber" idref="ABIp9">
      </param>
      <param name="szTaxId" idref="ABIp10">
      </param>
      <param name="szSearchType" idref="ABIp11">
      </param>
      <param name="szAlphaName" idref="ABIp12">
      </param>
      <param name="szSecondaryAlphaName" idref="ABIp13">
      </param>
      <param name="szMailingName" idref="ABIp14">
      </param>
      <param name="szSecondaryMailingName" idref="ABIp15">
      </param>
      <param name="szDescriptionCompressed" idref="ABIp16">
      </param>
      <param name="szBusinessUnit" idref="ABIp17">
      </param>
      <param name="szAddressLine1" idref="ABIp18">
      </param>
      <param name="szAddressLine2" idref="ABIp19">
      </param>
      <param name="szAddressLine3" idref="ABIp20">
      </param>
      <param name="szAddressLine4" idref="ABIp21">
      </param>
      <param name="szPostalCode" idref="ABIp22">
      </param>
      <param name="szCity" idref="ABIp23">
      </param>
      <param name="szCounty" idref="ABIp24">
      </param>
      <param name="szState" idref="ABIp25">
      </param>
      <param name="szCountry" idref="ABIp26">
      </param>
      <param name="szCarrierRoute" idref="ABIp27">
      </param>
      <param name="szBulkMailingCenter" idref="ABIp28">
      </param>
      <param name="szPrefix1" idref="ABIp29">
      </param>
      <param name="szPhoneNumber1" idref="ABIp30">
      </param>
      <param name="szPhoneNumberType1" idref="ABIp31">
      </param>
      <param name="szPhoneAreaCode2" idref="ABIp32">
      </param>
      <param name="szPhoneNumber2" idref="ABIp33">
      </param>
      <param name="szPhoneNumberType2" idref="ABIp34">
      </param>
      <param name="cPayablesYNM" idref="ABIp35">
      </param>
      <param name="cReceivablesYN" idref="ABIp36">
      </param>
      <param name="cEmployeeYN" idref="ABIp37">
      </param>
      <param name="cUserCode" idref="ABIp38">
      </param>
      <param name="cARAPNettingY" idref="ABIp39">
      </param>
      <param name="cSubledgerInactiveCode" idref="ABIp40">
      </param>
      <param name="cPersonCorporationCode" idref="ABIp41">
      </param>
      <param name="szCertificate" idref="ABIp42">
      </param>
      <param name="szAddlIndTaxID" idref="ABIp43">
      </param>
      <param name="szCreditMessage" idref="ABIp44">
      </param>
      <param name="szLanguage" idref="ABIp45">
      </param>
      <param name="szIndustryClassification" idref="ABIp46">
      </param>
      <param name="cEMail" idref="ABIp47">
      </param>
      <param name="mn1stAddressNumber" idref="ABIp48">
      </param>
      <param name="mn2ndAddressNumber" idref="ABIp49">
      </param>
      <param name="mn3rdAddressNumber" idref="ABIp50">
      </param>
      <param name="mn4thAddressNumber" idref="ABIp51">
      </param>
      <param name="mn5thAddressNumber" idref="ABIp52">
      </param>
      <param name="mnFactorSpecialPayee" idref="ABIp53">
      </param>
      <param name="mnParentNumber" idref="ABIp54">
      </param>
      <param name="cAddressType3YN" idref="ABIp55">
      </param>
      <param name="cAddressType4YN" idref="ABIp56">
      </param>
      <param name="cAddressType5YN" idref="ABIp57">
      </param>
      <param name="szCategoryCode01" idref="ABIp58">
      </param>
      <param name="szAccountRepresentative" idref="ABIp59">
      </param>
      <param name="szCategoryCode03" idref="ABIp60">
      </param>
      <param name="szGeographicRegion" idref="ABIp61">
      </param>
      <param name="szCategoryCode05" idref="ABIp62">
      </param>
      <param name="szCategoryCode06" idref="ABIp63">
      </param>
      <param name="sz1099Reporting" idref="ABIp64">
      </param>
      <param name="szCategoryCode08" idref="ABIp65">
      </param>
      <param name="szCategoryCode09" idref="ABIp66">
      </param>
      <param name="szCategoryCode10" idref="ABIp67">
      </param>
      <param name="szSalesRegion" idref="ABIp68">
      </param>
      <param name="szCategoryCode12" idref="ABIp69">
      </param>
      <param name="szLineOfBusiness" idref="ABIp70">
      </param>
      <param name="szSalesVolume" idref="ABIp71">
      </param>
      <param name="szCategoryCode15" idref="ABIp72">
      </param>
      <param name="szCategoryCode16" idref="ABIp73">
      </param>
      <param name="szCategoryCode17" idref="ABIp74">
      </param>
      <param name="szCategoryCode18" idref="ABIp75">
      </param>
      <param name="szCategoryCode19" idref="ABIp76">
      </param>
      <param name="szCategoryCode20" idref="ABIp77">
      </param>
      <param name="szCategoryCode21" idref="ABIp78">
      </param>
      <param name="szCategoryCode22" idref="ABIp79">
      </param>
      <param name="szCategoryCode23" idref="ABIp80">
      </param>
      <param name="szCategoryCode24" idref="ABIp81">
      </param>
      <param name="szCategoryCode25" idref="ABIp82">
      </param>
      <param name="szCategoryCode26" idref="ABIp83">
      </param>
      <param name="szCategoryCode27" idref="ABIp84">
      </param>
      <param name="szCategoryCode28" idref="ABIp85">
      </param>
      <param name="szCategoryCode29" idref="ABIp86">
      </param>
      <param name="szCategoryCode30" idref="ABIp87">
      </param>
      <param name="szGlBankAccount" idref="ABIp88">
      </param>
      <param name="mnTimeScheduledIn" idref="ABIp89">
      </param>
      <param name="jdDateScheduledIn" idref="ABIp90">
      </param>
      <param name="cClearedY" idref="ABIp91">
      </param>
      <param name="szRemark" idref="ABIp92">
      </param>
      <param name="szUserReservedCode" idref="ABIp93">
      </param>
      <param name="jdUserReservedDate" idref="ABIp94">
      </param>
      <param name="mnUserReservedAmount" idref="ABIp95">
      </param>
      <param name="mnUserReservedNumber" idref="ABIp96">
      </param>
      <param name="szUserReservedReference" idref="ABIp97">
      </param>
      <param name="jdDateEffective" idref="ABIp98">
      </param>
      <param name="szProgramId" idref="ABIp99">
      </param>
      <param name="szRemark1" idref="ABIp100">
      </param>
      <param name="mnAddNumParentOriginal" idref="ABIp101">
      </param>
      <param name="OKToDelete" idref="ABIp102">
      </param>
      <param name="szVersionconsolidated" idref="ABIp103">
      </param>
      <param name="cDirectionIndicator" idref="ABIp104">
      </param>
      <param name="cEdiSuccessfullyProcess" idref="ABIp105">
      </param>
      <param name="szCountryForPayroll" idref="ABIp106">
      </param>
      <param name="szShortcutClientType" idref="ABIp107">
      </param>
      <param name="szTicker" idref="ABIp108">
      </param>
      <param name="szStockExchange" idref="ABIp109">
      </param>
      <param name="szDUNSNumber" idref="ABIp110">
      </param>
      <param name="szClassificationCode01" idref="ABIp111">
      </param>
      <param name="szClassificationCode02" idref="ABIp112">
      </param>
      <param name="szClassificationCode03" idref="ABIp113">
      </param>
      <param name="szClassificationCode04" idref="ABIp114">
      </param>
      <param name="szClassificationCode05" idref="ABIp115">
      </param>
      <param name="mnNumberOfEmployee" idref="ABIp116">
      </param>
      <param name="mnGrowthRate" idref="ABIp117">
      </param>
      <param name="szYearStarted" idref="ABIp118">
      </param>
      <param name="szEmployeeGroupApprovals" idref="ABIp119">
      </param>
      <param name="cIndicatorFlg" idref="ABIp120">
      </param>
      <param name="szRevenueRange" idref="ABIp121">
      </param>
      <param name="mnABSynchStatus" idref="ABIp122">
      </param>
      <param name="mnABServerStatus" idref="ABIp123">
      </param>
      <param name="mnPhone1SynchStatus" idref="ABIp124">
      </param>
      <param name="mnPhone1ServerStatus" idref="ABIp125">
      </param>
      <param name="mnPhone2SynchStatus" idref="ABIp126">
      </param>
      <param name="mnPhone2ServerStatus" idref="ABIp127">
      </param>
      <param name="mnAddressSynchStatus" idref="ABIp128">
      </param>
      <param name="mnAddressServerStatus" idref="ABIp129">
      </param>
      <param name="mnParentAddSynchStatus" idref="ABIp130">
      </param>
      <param name="mnPreviousF0101ErrorStatus" idref="ABIp131">
      </param>
      <param name="mnWhosWhoSynchStatus" idref="ABIp132">
      </param>
      <param name="mnWhosWhoServerStatus" idref="ABIp133">
      </param>
      <param name="cCallSalesTeamAlignment" idref="ABIp134">
      </param>
    </params>
  </callMethod>
</jdeRequest>
         * 
         */

        /// <summary>
        /// http://localhost:60576/api/Connector/GetJDData/   ///?PostJDEXMLRequest=<xml></xml>
        /// </summary>
        /// <param name="PostJDEXMLRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        [ActionName("GetJDData")]
        public string Get()
        {
            try
            {
                try
                {

                    HttpContent requestContent = Request.Content;
                    string PostJDEXMLRequest = requestContent.ReadAsStringAsync().Result;


                    JDAPI.JDECaller JDCaller = new JDAPI.JDECaller();

                    JDCaller.ReportStatus += (string status, JDAPI.ConnectionInfo connectionInfo) =>
                    {
                        WebApiApplication.RequestLogger.Info(string.Format("           Step: {0} Stats: {1}{2}", status, connectionInfo.ToString(), Environment.NewLine));
                    };

                    WebApiApplication.RequestLogger.Info(string.Format("Request started At {0}", DateTime.Now));
                    WebApiApplication.RequestLogger.Info(string.Format("   Client Request"));
                    WebApiApplication.RequestLogger.Info(string.Format("       {0}", PostJDEXMLRequest));


                    JDAPI.ConnectionInfo ConInfo = JDCaller.GetConnection();

                    //Inject attributes
                    var StuffedRequest = JDAPI.JDConnectorHelper.FixRequest(PostJDEXMLRequest, ConInfo);
                    WebApiApplication.RequestLogger.Info(string.Format("   Stuffed"));
                    WebApiApplication.RequestLogger.Info(string.Format("       {0}", StuffedRequest));


                    //Call JDE com Component
                    var Response = JDCaller.CallJDE(StuffedRequest, ConInfo);
                    WebApiApplication.RequestLogger.Info(string.Format("   JD Response"));
                    WebApiApplication.RequestLogger.Info(string.Format("       {0}", Response));


                    //Remove attributes
                    var CleanResponse = JDAPI.JDConnectorHelper.FixResponse(Response);
                    WebApiApplication.RequestLogger.Info(string.Format("   JdReponse Cleaned"));
                    WebApiApplication.RequestLogger.Info(string.Format("       {0}", CleanResponse));
                    WebApiApplication.RequestLogger.Info(string.Format("Request Finished At {0}", DateTime.Now));


                    //Send Clean Response
                    return CleanResponse;
                }
                catch (Exception ex)
                {
                    return string.Format("<Error> <Code>{0}</Code> <Message>{2}</Message><StackTrace>{1}</StackTrace></Error>",0, ex.Message, ex.StackTrace);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                return pageContent;
            }

        }

    }
}
