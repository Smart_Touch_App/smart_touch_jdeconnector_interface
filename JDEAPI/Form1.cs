﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JDEAPI
{
    public partial class Form1 : Form
    {

        public static ILog RequestLogger;
        public static ILog SummaryLogger;

        public Form1()
        {
            InitializeComponent();
            SetLogs();
            JDAPI.ConnectionPool.SummaryLogger = SummaryLogger;

            JDAPI.ConnectionPool.StartPool();
            JDAPI.ConnectionPool.StartMonitor();
        }


        private void SetLogs()
        {

            log4net.Appender.RollingFileAppender roller = new log4net.Appender.RollingFileAppender();
            roller.Name = "LogRequest";
            roller.File = "LogRequests";
            roller.DatePattern = "";

            roller.AppendToFile = true;
            roller.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Composite;
            roller.MaxSizeRollBackups = 100;
            roller.MaximumFileSize = "10mb";
            roller.StaticLogFileName = true;
            roller.Layout = new log4net.Layout.PatternLayout();
            roller.ActivateOptions();
            RequestLogger = LogManager.GetLogger("FileApp");


            RequestLogger = LogManager.GetLogger("LogRequest");
            var l = (log4net.Repository.Hierarchy.Logger)RequestLogger.Logger;
            l.AddAppender(roller);
            l.Hierarchy.Configured = true;




            log4net.Appender.RollingFileAppender roller1 = new log4net.Appender.RollingFileAppender();
            roller1.Name = "LogSummary";
            roller1.File = "LogSummary";
            roller1.DatePattern = "";

            roller1.AppendToFile = true;
            roller1.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Composite;
            roller1.MaxSizeRollBackups = 100;
            roller1.MaximumFileSize = "10mb";
            roller1.StaticLogFileName = true;
            roller1.Layout = new log4net.Layout.PatternLayout();
            roller1.ActivateOptions();
            SummaryLogger = LogManager.GetLogger("LogSummary");
            var l1 = (log4net.Repository.Hierarchy.Logger)SummaryLogger.Logger;
            l1.AddAppender(roller1);
            l1.Hierarchy.Configured = true;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            JDAPI.JDECaller JDCaller = new JDAPI.JDECaller();
            JDAPI.ConnectionInfo ConInfo = JDCaller.GetConnection();

            //Inject attributes before request sent to JD Server
            var StuffedRequest = JDAPI.JDConnectorHelper.FixRequest(txtClientRequest.Text, ConInfo);

            //Call JDE com Component

            var JDEResponse = JDCaller.CallJDE(StuffedRequest, ConInfo);

            //Remove attributes before response sent to client
            var CleanResponse = JDAPI.JDConnectorHelper.FixResponse(JDEResponse);

            //Send Clean Response
            txtFixedRequest.Text = StuffedRequest;
            txtJDEResponse.Text = JDEResponse;
            txtFixedResponse.Text = CleanResponse;




        }

        private void btnRunUsingService_Click(object sender, EventArgs e)
        {
            string url = "http://localhost/JDConnector/api/Connector/GetJDData";
            url = "http://localhost:60576/api/Connector/GetJDData/";
            //Inject attributes before request sent to JD Server
            var CleanResponse = ServiceClient.ConsumeJDEConnectorService(txtClientRequest.Text, url);

      
            //Send Clean Response
            txtFixedResponse.Text = CleanResponse;
        }

        private void btnCreatePool_Click(object sender, EventArgs e)
        {

            JDAPI.ConnectionPool.StartPool();
            var Pool = new JDAPI.ConnectionPool();
            var pools = Pool.Pool;
        }

     
    }
}
