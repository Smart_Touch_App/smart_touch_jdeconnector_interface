﻿using log4net;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extension;

namespace JDAPI
{
    /*
        1. Idle time
        2. After x (50) no. of request - the pool object will expire
        3. Log file - mark up req - all 4 kinds of xml to log - Done

     */


    public struct PoolInfo
    {
        public DateTime CreatedOn;

    }

    public struct ConnectionStat
    {
        public int NoOfTimesUsed;
        public DateTime LastUsedOn;
        public bool IsExpired;
    }

    public class ConnectionInfo
    {


        public xmlinterop_comLib.XmlinteropClass JDEConnection { get; set; }


        private string _SessionQuery = @"<?xml version='1.0' encoding='UTF-8' ?>
<jdeRequest pwd='FBMOB13!' role='*ALL' type='callmethod' user='MOBILEWS' comment='' session='' environment='PY910' sessionidle='300'>
	<callMethod app='XMLTest' name='GetUDC'>
		<params>
			<param name='szSystemCode'>01</param>
			<param name='szRecordTypeCode'>01</param>
			<param name='szUserDefinedCode'>CCP</param>
			<param name='cSuppressErrorMessage'>1</param>
		</params>
	</callMethod>
</jdeRequest>";
        public string User { get; set; }
        public string Host { get; set; }
        public short Port { get; set; }

        public string Session { get; set; }
        public string Environment { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string xType { get; set; }
        public string Comment { get; set; }
        public short SessionIdle { get; set; }
        public string SessionQuery { get { return _SessionQuery; } }

        public ConnectionStat Stats;


        new public string ToString()
        {

            return string.Format("SessionID {0}  Used (NOS) {1}  LastUsed {2} ", Session, Stats.NoOfTimesUsed, Stats.LastUsedOn);
        }




    }

    public class ConnectionPool
    {

        private static ObjectPool<ConnectionInfo> _Pool = new ObjectPool<ConnectionInfo>();

        //pwd='FBMOB13!' role='*ALL' type='callmethod' user='MOBILEWS' comment='' session='' environment='PY910' sessionidle='300'>

        private static int _InitialPoolSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["InitialPoolSize"]);
        private static int _incrementPoolBy = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["IncrementPoolBy"]);
        private static int _MaxUsePerConnectionAllowed = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxUsePerConnectionAllowed"]);
        private static int _maxPoolSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxPoolSize"]);

        //Refreshed when initializing new objects
        private static string _host = System.Configuration.ConfigurationManager.AppSettings["JDHost"];
        private static short _port = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["JDPort"]);
        private static short _JDIdleTimeOutInSeconds = Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["JDIdleTimeOutInSeconds"]);
        private static string _Role = System.Configuration.ConfigurationManager.AppSettings["Role"];
        private static string _JDEnvironment = System.Configuration.ConfigurationManager.AppSettings["JDEnvironment"];
        private static string _JDRequestType = System.Configuration.ConfigurationManager.AppSettings["JDRequestType"];
        private static string _User = System.Configuration.ConfigurationManager.AppSettings["User"];
        private static string _Comment = System.Configuration.ConfigurationManager.AppSettings["Comment"];
        private static string _Password = System.Configuration.ConfigurationManager.AppSettings["Password"];



        public static ILog SummaryLogger { get; set; }

        public void setPoolDefaults(int initialPoolSize = 5, int incrementPoolBy = 5, int maxPoolSize = 20,
            int maxIdleTimeInMinutes = 30, int maxUsePerConnectionAllowed = 50, short JDIdleTimeOutInSeconds = 300, string JDHost = "WTES0053", short JDPort = 8053)
        {
            _InitialPoolSize = initialPoolSize;
            _incrementPoolBy = incrementPoolBy;
            _maxPoolSize = maxPoolSize;
            _MaxUsePerConnectionAllowed = maxUsePerConnectionAllowed;
            _JDIdleTimeOutInSeconds = JDIdleTimeOutInSeconds;


            _host = JDHost;
            _port = JDPort;
        }

  

        public ObjectPool<ConnectionInfo> Pool { get { return _Pool; } }

        public static void StartPool()
        {
            AddToPool(_InitialPoolSize);
        }

        private static void AddToPool(int noOfObjectsToAdd)
        {
            SummaryLogger.Info(string.Format("Creating Pool At {0}", DateTime.Now));
            for (int i = 0; i < noOfObjectsToAdd; i++)
            {
                var conn = InitializeObject();
                _Pool.PutObject(conn);
                SummaryLogger.Info(string.Format("Created New Pool Memeber {0}", conn.ToString()));
            }


            SummaryLogger.Info(string.Format("Pool Created At {0}", DateTime.Now));
        }

        private static ConnectionInfo InitializeObject()
        {
            //Active connection must continue to live by old settings
            //RefreshConfigs();

            var ConnInfo = new ConnectionInfo();
            SetAttributes(ConnInfo);
            var RequestQuery = ConnInfo.SessionQuery;
            RequestQuery = JDConnectorHelper.FixRequest(RequestQuery, ConnInfo);

            string Response = string.Empty;
            ConnInfo.JDEConnection = new xmlinterop_comLib.XmlinteropClass();
            ConnInfo.JDEConnection.jdeXmlRequest(RequestQuery, out Response, ConnInfo.Host, ConnInfo.Port, ConnInfo.SessionIdle);
            ConnInfo.Session = JDConnectorHelper.GetValue(Response, "session");


            return ConnInfo;
        }

        private static void SetAttributes(ConnectionInfo ConnInfo)
        {
            ConnInfo.User = _User;
            ConnInfo.Role = _Role;
            ConnInfo.Comment = _Comment;
            ConnInfo.Environment = _JDEnvironment;
            ConnInfo.Password = _Password;
            ConnInfo.xType = _JDRequestType;
            ConnInfo.Session = "";
            ConnInfo.Host = _host;
            ConnInfo.Port = _port;
            ConnInfo.SessionIdle = _JDIdleTimeOutInSeconds;


            ConnInfo.Stats.LastUsedOn = DateTime.Now;
            ConnInfo.Stats.NoOfTimesUsed = 0;


        }

        public ConnectionInfo GetJDObject()
        {
            while (!System.Threading.Monitor.TryEnter(_Pool)) { }

            if (_Pool.Count() == 0)
            {
                //Start off with new config
                IsConfigChanged(shouldReplace: true);
                AddToPool(_incrementPoolBy);
            }

            var Conn = _Pool.GetObject();
            if (Conn != null)
            {
                Conn.Stats.NoOfTimesUsed++;
                Conn.Stats.LastUsedOn = DateTime.Now;
            }
            else
            {

                Conn = InitializeObject();
                Conn.Stats.NoOfTimesUsed++;
                Conn.Stats.LastUsedOn = DateTime.Now;
            }
            System.Threading.Monitor.Exit(_Pool);

            return Conn;

        }

        public void PutJDObject(ConnectionInfo jdobject)
        {
            while (!System.Threading.Monitor.TryEnter(_Pool)) { }
            /*
               1. Do not add back to pool if adding makes pool size larger than Max Pool size Defined
               2. Do not add back to pool if the object's max use has been been reached
             */
            if (jdobject.Stats.NoOfTimesUsed < _MaxUsePerConnectionAllowed && _Pool.PoolObjects.Count < _maxPoolSize) //Not Expired by Usage
            {
                //In-Active Connection, Do not add back if configuration changed, 
                //the pool will slowly go down to zero and then increment will begin with new settings at the time of getting new object
                if (!IsConfigChanged(shouldReplace: false))
                    _Pool.PutObject(jdobject);
            }
            System.Threading.Monitor.Exit(_Pool);
        }


        public static bool IsConfigChanged(bool shouldReplace)
        {
            Boolean IsChanged = false;
            bool IsChangeAny = false;

            _InitialPoolSize = _InitialPoolSize.IsEqualThenReplace(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["InitialPoolSize"]), out IsChanged, shouldReplace, ref IsChangeAny);
            _incrementPoolBy = _incrementPoolBy.IsEqualThenReplace(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["IncrementPoolBy"]), out IsChanged, shouldReplace, ref IsChangeAny);
            _MaxUsePerConnectionAllowed = _MaxUsePerConnectionAllowed.IsEqualThenReplace(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxUsePerConnectionAllowed"]), out IsChanged, shouldReplace, ref IsChangeAny);
            _maxPoolSize = _maxPoolSize.IsEqualThenReplace(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxPoolSize"]), out IsChanged, shouldReplace, ref IsChangeAny);

            //Refreshed when initializing new objects
            _host = _host.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["JDHost"].ToString(), out IsChanged, shouldReplace, ref IsChangeAny);
            _port = _port.IsEqualThenReplace(Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["JDPort"]), out IsChanged, shouldReplace, ref IsChangeAny);
            _JDIdleTimeOutInSeconds = _JDIdleTimeOutInSeconds.IsEqualThenReplace(Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["JDIdleTimeOutInSeconds"]), out IsChanged, shouldReplace, ref IsChangeAny);
            _Role = _Role.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["Role"], out IsChanged, shouldReplace, ref IsChangeAny);
            _JDEnvironment = _JDEnvironment.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["JDEnvironment"], out IsChanged, shouldReplace, ref IsChangeAny);
            _JDRequestType = _JDRequestType.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["JDRequestType"], out IsChanged, shouldReplace, ref IsChangeAny);
            _User = _User.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["User"], out IsChanged, shouldReplace, ref IsChangeAny);
            _Comment = _Comment.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["Comment"], out IsChanged, shouldReplace, ref IsChangeAny);
            _Password = _Password.IsEqualThenReplace(System.Configuration.ConfigurationManager.AppSettings["Password"], out IsChanged, shouldReplace, ref IsChangeAny);
            if (IsChangeAny)
            {
                return true;
            }
            return false;
        }

        public static void StartMonitor()
        {
            var task = Task.Factory.StartNew(() =>
            {

                while (true)
                {
                    //Sleep for 5 seconds 
                    System.Threading.Thread.Sleep(_JDIdleTimeOutInSeconds * 1000 - 10000);
                    SummaryLogger.Info(string.Format("Starting Monitoring Pool At {0}", DateTime.Now));
                   // if (_Pool.PoolObjects.Where(x => x.Stats.NoOfTimesUsed >= _MaxUsePerConnectionAllowed).Count() > 0
                    //    || _Pool.PoolObjects.Where(x => (DateTime.Now - x.Stats.LastUsedOn).Seconds >= _JDIdleTimeOutInSeconds - 10).Count() > 0
                    //    )
                    {
                        RecyclePool();
                    }

                }

            }, TaskCreationOptions.LongRunning);

        }

        private static void RecyclePool()
        {
            string xlock = string.Empty;
            //obtain a Lock
            while (!System.Threading.Monitor.TryEnter(xlock)) { }

            try
            {
                SummaryLogger.Info(string.Format("Starting Pool Recycle At {0}", DateTime.Now));

                ObjectPool<ConnectionInfo> NewPool = new ObjectPool<ConnectionInfo>();
                while (_Pool.Count() > 0)
                {
                    ConnectionInfo obj = new ConnectionInfo();
                    if (_Pool.PoolObjects.TryTake(out obj))
                    {
                       // if (obj.Stats.NoOfTimesUsed >= _MaxUsePerConnectionAllowed
                       //     || (DateTime.Now - obj.Stats.LastUsedOn).Seconds > _JDIdleTimeOutInSeconds - 10)
                        //{
                        SummaryLogger.Info(string.Format("Recycling Expired Pool Memeber: {0}", obj.ToString()));
                        //ReInitialize
                        obj = InitializeObject();
                        //Add to new collection
                        NewPool.PutObject(obj);
                        SummaryLogger.Info(string.Format("Replaced with new Pool Memeber: {0}", obj.ToString()));
                        //}
                        //else
                        //{
                        //    //Do not Initialize and AddTo New Collection
                        //    NewPool.PutObject(obj);
                        //    SummaryLogger.Info(string.Format("Active Pool Memeber Retained: {0}", obj.ToString()));
                        //}

                    }
                }

                //Create the pool with reiniialized members
                _Pool = new ObjectPool<ConnectionInfo>();
                foreach (var obj in NewPool.PoolObjects)
                {
                    _Pool.PutObject(obj);
                    SummaryLogger.Info(string.Format("Pool Memeber {0}", obj.ToString()));

                }
                SummaryLogger.Info(string.Format("Pool Refresh Done At {0}", DateTime.Now));

            }
            catch
            {

                throw;
            }
            finally
            {
                //Release the lock
                System.Threading.Monitor.Exit(xlock);

            }

        }

    }



    public class ObjectPool<T>
    {
        private ConcurrentBag<T> _objects;
        private Func<T> _objectGenerator;

        public ObjectPool()
        {
            _objects = new ConcurrentBag<T>();
        }
        public ObjectPool(Func<T> objectGenerator)
        {
            if (objectGenerator == null) throw new ArgumentNullException("objectGenerator");
            _objects = new ConcurrentBag<T>();
            _objectGenerator = objectGenerator;
        }

        public void Remove(Func<ConcurrentBag<T>> Expired)
        {
            //_objects.Where(x => x.Stats.NoOfTimesUsed > _MaxUsePerConnectionAllowed
            //                        || (DateTime.Now - x.Stats.LastUsedOn).Minutes > _MaxIdleTimeInMinutes).Take(1);

        }

        public ConcurrentBag<T> PoolObjects
        {
            get { return _objects; }
        }

        public T GetObject()
        {
            T item;
            if (_objects.TryTake(out item)) return item;
            return default(T);
        }

        public void PutObject(T item)
        {
            _objects.Add(item);
        }
        public int Count()
        {
            return _objects.Count;
        }
    }
}
