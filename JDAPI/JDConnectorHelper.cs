﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace JDAPI
{
    public class JDConnectorHelper
    {
        public static string GetSession(string xmlRequest)
        {
            return GetValue(xmlRequest,"session");
        }

        public static string GetValue(string xmlRequest,string attrName)
        {
            string ResultValue = string.Empty;
            var root = XElement.Parse(xmlRequest, LoadOptions.PreserveWhitespace);
            var val = root.Attributes().Where(x => x.Name == attrName).FirstOrDefault().Value;
            return val;
        }

        public static string GetUser(string xmlRequest)
        {
            string ResultValue = string.Empty;
            var root = XElement.Parse(xmlRequest, LoadOptions.PreserveWhitespace);
            var val = root.Attributes().Where(x => x.Name == "user").FirstOrDefault().Value;
            return val;
        }

        public static string FixRequest(string xmlRequest,ConnectionInfo ci)
        {
           // xmlRequest = xmlRequest.Replace("\n", "");
            string ResultValue = string.Empty;
            var root = XElement.Parse(xmlRequest, LoadOptions.PreserveWhitespace);
            root.RemoveAttributes();
         
            //pwd='FBMOB13!' role='*ALL' type='callmethod' user='MOBILEWS' comment='' session='' environment='PY910' sessionidle='300'
            root.SetAttributeValue("pwd", ci.Password);
            root.SetAttributeValue("role", ci.Role);
            root.SetAttributeValue("type", "callmethod");
            root.SetAttributeValue("user", ci.User);
            root.SetAttributeValue("comment", "");
            root.SetAttributeValue("session", ci.Session);
            root.SetAttributeValue("environment", ci.Environment);
            root.SetAttributeValue("sessionidle", ci.SessionIdle);
            return root.ToString();
        }

        public static string FixResponse(string xmlResponse)
        {
            string ResultValue = string.Empty;
            var root = XElement.Parse(xmlResponse, LoadOptions.PreserveWhitespace);
            root.RemoveAttributes();


            return root.InnerXML();
        }

    
      
    }

    public static class XElementExtension
    {
        public static string InnerXML(this XElement el)
        {
            var reader = el.CreateReader();
            reader.MoveToContent();
            return reader.ReadInnerXml().Replace(Convert.ToString(reader.QuoteChar), "'");
        }
    }
}
