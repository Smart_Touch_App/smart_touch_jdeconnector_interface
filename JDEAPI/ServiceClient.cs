﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JDEAPI
{
    public class ServiceClient
    {

        public static string ConsumeJDEConnectorService(string xmlRequest, string url)
        {

          
            string responseFromServer = string.Empty; ;


            //Send Request
            WebRequest request = WebRequest.Create(string.Format("{0}?PostJDEXMLRequest={1}", url, xmlRequest));
          //  request.Headers.Add("SOAPAction", "\"http://xxxxxx/\"");
            request.Method = "Get";

            //Get Response
            string response = string.Empty;

            using (System.Net.WebResponse resp = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(resp.GetResponseStream()))
                {
                    response = sr.ReadToEnd().Trim();
                }
            }

            return response;

        }
    }
}
