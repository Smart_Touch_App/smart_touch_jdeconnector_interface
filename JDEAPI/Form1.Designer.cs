﻿namespace JDEAPI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtFixedRequest = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFixedResponse = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtClientRequest = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtJDEResponse = new System.Windows.Forms.TextBox();
            this.txtSessionID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRunUsingService = new System.Windows.Forms.Button();
            this.btnCreatePool = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtFixedRequest
            // 
            this.txtFixedRequest.AllowDrop = true;
            this.txtFixedRequest.Location = new System.Drawing.Point(12, 240);
            this.txtFixedRequest.Multiline = true;
            this.txtFixedRequest.Name = "txtFixedRequest";
            this.txtFixedRequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFixedRequest.Size = new System.Drawing.Size(520, 222);
            this.txtFixedRequest.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 224);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Stuffed Request To JDE Server";
            // 
            // txtFixedResponse
            // 
            this.txtFixedResponse.AllowDrop = true;
            this.txtFixedResponse.Location = new System.Drawing.Point(541, 240);
            this.txtFixedResponse.Multiline = true;
            this.txtFixedResponse.Name = "txtFixedResponse";
            this.txtFixedResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFixedResponse.Size = new System.Drawing.Size(694, 222);
            this.txtFixedResponse.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(557, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Clean Response to Client";
            // 
            // txtClientRequest
            // 
            this.txtClientRequest.AllowDrop = true;
            this.txtClientRequest.Location = new System.Drawing.Point(15, 31);
            this.txtClientRequest.Multiline = true;
            this.txtClientRequest.Name = "txtClientRequest";
            this.txtClientRequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtClientRequest.Size = new System.Drawing.Size(520, 190);
            this.txtClientRequest.TabIndex = 4;
            this.txtClientRequest.Text = resources.GetString("txtClientRequest.Text");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Client Request";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(116, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Run Local";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtJDEResponse
            // 
            this.txtJDEResponse.AllowDrop = true;
            this.txtJDEResponse.Location = new System.Drawing.Point(541, 31);
            this.txtJDEResponse.Multiline = true;
            this.txtJDEResponse.Name = "txtJDEResponse";
            this.txtJDEResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtJDEResponse.Size = new System.Drawing.Size(713, 190);
            this.txtJDEResponse.TabIndex = 7;
            // 
            // txtSessionID
            // 
            this.txtSessionID.Location = new System.Drawing.Point(1077, 8);
            this.txtSessionID.Name = "txtSessionID";
            this.txtSessionID.Size = new System.Drawing.Size(169, 20);
            this.txtSessionID.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1017, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "SessionID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(538, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Response From JDE";
            // 
            // btnRunUsingService
            // 
            this.btnRunUsingService.Location = new System.Drawing.Point(236, 5);
            this.btnRunUsingService.Name = "btnRunUsingService";
            this.btnRunUsingService.Size = new System.Drawing.Size(121, 23);
            this.btnRunUsingService.TabIndex = 11;
            this.btnRunUsingService.Text = "Run Using Service";
            this.btnRunUsingService.UseVisualStyleBackColor = true;
            this.btnRunUsingService.Click += new System.EventHandler(this.btnRunUsingService_Click);
            // 
            // btnCreatePool
            // 
            this.btnCreatePool.Location = new System.Drawing.Point(376, 5);
            this.btnCreatePool.Name = "btnCreatePool";
            this.btnCreatePool.Size = new System.Drawing.Size(121, 23);
            this.btnCreatePool.TabIndex = 12;
            this.btnCreatePool.Text = "Create Pool";
            this.btnCreatePool.UseVisualStyleBackColor = true;
            this.btnCreatePool.Click += new System.EventHandler(this.btnCreatePool_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 576);
            this.Controls.Add(this.btnCreatePool);
            this.Controls.Add(this.btnRunUsingService);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSessionID);
            this.Controls.Add(this.txtJDEResponse);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtClientRequest);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtFixedResponse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFixedRequest);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFixedRequest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFixedResponse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtClientRequest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtJDEResponse;
        private System.Windows.Forms.TextBox txtSessionID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRunUsingService;
        private System.Windows.Forms.Button btnCreatePool;
    }
}

