﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDAPI
{
    public class JDECaller
    {
        public delegate void _ReportStatus(string status, ConnectionInfo connectionInfo);
        public event _ReportStatus ReportStatus;

        JDAPI.ConnectionPool Pool = new ConnectionPool();

        public ConnectionInfo GetConnection()
        {
            var ConnInfo = Pool.GetJDObject();
            if (ReportStatus != null)
                ReportStatus("GotObjectFromPool", ConnInfo);
            return ConnInfo;
        }

        public string CallJDE(string xmlRequest, ConnectionInfo ConnInfo)
        {
            string request = xmlRequest;
            string response=string.Empty;
            
            ConnInfo.JDEConnection.jdeXmlRequest(request, out response, ConnInfo.Host, ConnInfo.Port, ConnInfo.SessionIdle);

            Pool.PutJDObject(ConnInfo);
            if (ReportStatus != null)
                ReportStatus("PutObjectToPool", ConnInfo);


            return response;
        }

    }
}
