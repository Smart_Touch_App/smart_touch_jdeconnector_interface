﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extension
{
    public static class MethodExtension
    {

        public static string IsEqualThenReplace(this string left, string right, out bool isChanged, bool shouldReplace ,ref bool isChangeAny)
        {
            isChanged = false;
            isChangeAny = isChangeAny?true:false;
            if (left != right)
            {
                isChanged = true;
                isChangeAny = true;
                if (shouldReplace)
                    return right;
            }
            
            return left;
        }

        public static int IsEqualThenReplace(this int left, int right, out bool isChanged, bool shouldReplace, ref bool isChangeAny)
        {
            isChanged = false;
            isChangeAny = isChangeAny ? true : false;
            if (left != right)
            {
                isChanged = true;
                isChangeAny = true;
                if (shouldReplace)
                    return right;
            }

            return left;
        }

        public static decimal IsEqualThenReplace(this decimal left, decimal right, out bool isChanged, bool shouldReplace , ref bool isChangeAny)
        {
            isChanged = false;
            isChangeAny = isChangeAny ? true : false;
            if (left != right)
            {
                isChanged = true;
                isChangeAny = true;
                if (shouldReplace)
                    return right;
            }

            return left;
        }


        public static short IsEqualThenReplace(this short left, short right, out bool isChanged, bool shouldReplace, ref bool isChangeAny)
        {
            isChanged = false;
            isChangeAny = isChangeAny ? true : false;
            if (left != right)
            {
                isChanged = true;
                isChangeAny = true;
                if (shouldReplace)
                    return right;
            }

            return left;
        }

        public static float IsEqualThenReplace(this float left, float right, out bool isChanged, bool shouldReplace, ref bool isChangeAny)
        {
            isChanged = false;
            if (left != right)
            {
                isChanged = true;
                if (shouldReplace)
                    return right;
            }
            return left;
        }

        public static bool IsEqualThenReplace(this bool left, bool right, out bool isChanged, bool shouldReplace, ref bool isChangeAny)
        {
            isChanged = false;
            isChangeAny = isChangeAny ? true : false;
            if (left != right)
            {
                isChanged = true;
                isChangeAny = true;
                if (shouldReplace)
                    return right;
            }

            return left;
        }

    }
}
