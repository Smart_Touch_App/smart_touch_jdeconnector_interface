﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace JDConnector
{
    public class WebApiApplication : System.Web.HttpApplication
    {
       public static ILog RequestLogger;
       public static ILog SummaryLogger;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SetLogs();
            JDAPI.ConnectionPool.SummaryLogger = SummaryLogger;
            JDAPI.ConnectionPool.StartPool();
            JDAPI.ConnectionPool.StartMonitor();


        }

        private void SetLogs()
        {

            log4net.Appender.RollingFileAppender roller = new log4net.Appender.RollingFileAppender();
            roller.Name = "LogRequest";
            roller.File = "LogRequests";
            roller.DatePattern = "dd.MM.yyyy'.log'";

            roller.AppendToFile = true;
            roller.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Composite;
            roller.MaxSizeRollBackups = 100;
            roller.MaximumFileSize = "1MB";
            roller.StaticLogFileName = true;
            roller.Layout = new log4net.Layout.PatternLayout();
            roller.ActivateOptions();
            RequestLogger = LogManager.GetLogger("FileApp");


            RequestLogger = LogManager.GetLogger("LogRequest");
            var l = (log4net.Repository.Hierarchy.Logger)RequestLogger.Logger;
            l.AddAppender(roller);
            l.Hierarchy.Configured = true;




            log4net.Appender.RollingFileAppender roller1 = new log4net.Appender.RollingFileAppender();
            roller1.Name = "LogSummary";
            roller1.File = "LogSummary";
            roller1.DatePattern = "dd.MM.yyyy'.log'";

            roller1.AppendToFile = true;
            roller1.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Composite;
            roller1.MaxSizeRollBackups = 100;
            roller1.MaximumFileSize = "1MB";
            roller1.StaticLogFileName = true;
            roller1.Layout = new log4net.Layout.PatternLayout();
            roller1.ActivateOptions();
            SummaryLogger = LogManager.GetLogger("LogSummary");
            var l1 = (log4net.Repository.Hierarchy.Logger)SummaryLogger.Logger;
            l1.AddAppender(roller1);
            l1.Hierarchy.Configured = true;
        }
    }
}
